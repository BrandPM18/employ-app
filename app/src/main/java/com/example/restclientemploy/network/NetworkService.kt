package com.example.restclientemploy.network

import com.example.restclientemploy.domain.*
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface NetworkService {

    @POST("/employee/signIn")
    suspend fun signIn(@Body auth: Auth): Employee?

    @POST("/employee/signUp")
    suspend fun signUp(@Body user: Employee): Employee?

    @GET("/foodCategory")
    suspend fun getFoodCategories(): List<CategoryDto>

    @POST("/foodCategory")
    suspend fun newCategoryFood(@Body categoryDto: Category): CategoryDto?

    @DELETE("/foodCategory/{id}")
    suspend fun deleteFoodByCategory(@Path("id") id: Int): Void

    @GET("/food/category/{id}")
    suspend fun getFoodByCategory(@Path("id") id: Int): List<FoodDto>

    @POST("/food")
    suspend fun newFood(@Body foodDto: Food): FoodDto?

    @DELETE("/food/{id}")
    suspend fun deleteFood(@Path("id") id: Int): Void
}

object EmployNetwork {
    private const val BASE_URL = "http://192.168.1.12:8080"

    private val retrofitInstance =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(MoshiConverterFactory.create())
            .build()

    val NETWORK_INSTANCE: NetworkService = retrofitInstance.create(NetworkService::class.java)
}