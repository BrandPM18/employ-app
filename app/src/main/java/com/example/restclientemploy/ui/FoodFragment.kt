package com.example.restclientemploy.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.restclientemploy.R
import com.example.restclientemploy.databinding.FoodFragmentBinding
import com.example.restclientemploy.databinding.FoodItemBinding
import com.example.restclientemploy.domain.Food
import com.example.restclientemploy.viewmodels.FoodViewModel
import com.example.restclientemploy.viewmodels.SharedViewModel

class FoodFragment : Fragment() {

    private val viewModel: FoodViewModel by lazy {
        val sharedViewModel: SharedViewModel by activityViewModels()
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(
            this,
            FoodViewModel.Factory(activity.application, sharedViewModel)
        )
            .get(FoodViewModel::class.java)
    }

    private lateinit var binding: FoodFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.food_fragment, container, false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        val adapter = FoodAdapter()
        binding.foodsRecyclerView.adapter = adapter

        viewModel.getFood()

        viewModel.foods.observe(viewLifecycleOwner, {
            adapter.submitList(it)
        })

        viewModel.showToast.observe(viewLifecycleOwner, {
            if (it) {
                Toast.makeText(
                    requireContext(),
                    "Foodo agregado",
                    Toast.LENGTH_SHORT
                ).show()

                viewModel.updateShowToast(false)
            }
        })
    }

    inner class FoodAdapter :
        ListAdapter<Food, FoodAdapter.ViewHolder>(DiffCallback()) {

        inner class ViewHolder(private val binding: FoodItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(item: Food) {
                binding.food = item
                binding.imageView.setImageBitmap(item.image)
                binding.viewModel = viewModel
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = FoodItemBinding.inflate(inflater, parent, false)
            return ViewHolder(binding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bind(getItem(position))
    }

    class DiffCallback : DiffUtil.ItemCallback<Food>() {
        override fun areItemsTheSame(oldItem: Food, newItem: Food): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Food, newItem: Food): Boolean {
            return oldItem == newItem
        }

    }
}