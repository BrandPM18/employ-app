package com.example.restclientemploy.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.restclientemploy.R
import com.example.restclientemploy.databinding.LoginFragmentBinding
import com.example.restclientemploy.viewmodels.LoginViewModel
import com.example.restclientemploy.viewmodels.SharedViewModel

class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by lazy {
        val sharedViewModel: SharedViewModel by activityViewModels()
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(
            this,
            LoginViewModel.Factory(activity.application, sharedViewModel)
        )
            .get(LoginViewModel::class.java)
    }

    private lateinit var binding: LoginFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.login_fragment, container, false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        binding.emailTextInputEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.id = text.toString().toInt()
        }

        binding.passwordTextInputEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.password = text.toString()
        }

        viewModel.invalidCredentials.observe(viewLifecycleOwner, {
            if (it) {
                Toast.makeText(
                    requireContext(),
                    "El usuario o contraseña son incorrectos",
                    Toast.LENGTH_SHORT
                ).show()

                viewModel.updateInvalidCredentials(false)
            }
        })

        viewModel.successLogin.observe(viewLifecycleOwner, {
            if (it) {
                val action = LoginFragmentDirections.actionLoginFragmentToCategoriesFragment()
                findNavController().navigate(action)
                viewModel.clear()
            }
        })
    }
}