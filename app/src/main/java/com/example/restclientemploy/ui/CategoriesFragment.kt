package com.example.restclientemploy.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.restclientemploy.R
import com.example.restclientemploy.databinding.CategoriesFragmentBinding
import com.example.restclientemploy.databinding.CategoryItemBinding
import com.example.restclientemploy.domain.Category
import com.example.restclientemploy.viewmodels.CategoriesViewModel
import com.example.restclientemploy.viewmodels.SharedViewModel

class CategoriesFragment : Fragment() {

    private val viewModel: CategoriesViewModel by lazy {
        val sharedViewModel: SharedViewModel by activityViewModels()
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(
            this,
            CategoriesViewModel.Factory(activity.application, sharedViewModel)
        )
            .get(CategoriesViewModel::class.java)
    }

    private lateinit var binding: CategoriesFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.categories_fragment, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        val adapter = CategoryAdapter()
        binding.categoriesRecyclerView.adapter = adapter

        viewModel.getCategories()

        viewModel.categories.observe(viewLifecycleOwner, {
            adapter.submitList(it)
        })

        viewModel.toFoodsView.observe(viewLifecycleOwner, {
            if (it) {
                val action =
                    CategoriesFragmentDirections.actionCategoriesFragmentToFoodFragment()
                findNavController().navigate(action)
                viewModel.clear()
            }
        })
    }

    inner class CategoryAdapter :
        ListAdapter<Category, CategoryAdapter.ViewHolder>(DiffCallback()) {

        inner class ViewHolder(private val binding: CategoryItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(item: Category) {
                binding.category = item
                binding.viewModel = viewModel
                binding.categoryImageView.setImageBitmap(item.image)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = CategoryItemBinding.inflate(inflater, parent, false)
            return ViewHolder(binding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bind(getItem(position))
    }

    class DiffCallback : DiffUtil.ItemCallback<Category>() {
        override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem == newItem
        }

    }
}