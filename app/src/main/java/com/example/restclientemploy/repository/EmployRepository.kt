package com.example.restclientemploy.repository

import android.graphics.BitmapFactory
import android.util.Base64
import com.example.restclientemploy.domain.Auth
import com.example.restclientemploy.domain.Category
import com.example.restclientemploy.domain.Employee
import com.example.restclientemploy.domain.Food
import com.example.restclientemploy.network.EmployNetwork

class EmployRepository {

    suspend fun signIn(id: Int, password: String): Employee? {
        return try {
            EmployNetwork.NETWORK_INSTANCE.signIn(Auth(id, password))
        } catch (e: Exception) {
            throw e
        }
    }

    suspend fun signUp(id: Int, password: String): Employee? {
        return try {
            EmployNetwork.NETWORK_INSTANCE.signUp(Employee( id, password))
        } catch (e: Exception) {
            null
        }
    }

    suspend fun getCategories(): List<Category> {
        return try {
            val categories = EmployNetwork.NETWORK_INSTANCE.getFoodCategories()

            categories.map {
                val imageBytes = Base64.decode(it.image, 0)
                val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                Category(it.id, it.description,image)
            }
        } catch (e: Exception) {
            throw e
            emptyList()
        }
    }

    suspend fun getFoodByCategory(category: Int): List<Food> {
        return try {
            val foodList = EmployNetwork.NETWORK_INSTANCE.getFoodByCategory(category)

            foodList.map {
                val imageBytes = Base64.decode(it.image, 0)
                val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                Food(it.name, it.description, "S/. ${it.price}", image)
            }
        } catch (e: Exception) {
            emptyList()
        }
    }

    suspend fun deleteFood(category: Int): Void {
        return try {
             EmployNetwork.NETWORK_INSTANCE.deleteFood(category)
        } catch (e: Exception) {
            throw e
        }
    }
}