package com.example.restclientemploy.domain

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FoodDto (
    var id: Int,
    var name: String,
    var description: String,
    var price: Float,
    var image: String
)