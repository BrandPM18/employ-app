package com.example.restclientemploy.domain

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CategoryDto(
    var id: Int,
    var description: String,
    var image: String
)