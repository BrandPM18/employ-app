package com.example.restclientemploy.domain

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Employee (
    var id: Int,
    var password: String
    )
