package com.example.restclientemploy.domain

import android.graphics.Bitmap


data class Category(
    var id: Int,
    var title: String,
    var image: Bitmap
)