package com.example.restclientemploy.domain

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Auth(
    var user: Int,
    var password: String
)