package com.example.restclientemploy.domain

import android.graphics.Bitmap


data class Food  (
    var title: String,
    var description: String,
    var price: String,
    var image: Bitmap
)