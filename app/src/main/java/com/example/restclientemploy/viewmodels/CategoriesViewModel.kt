package com.example.restclientemploy.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.example.restclientemploy.domain.Category
import kotlinx.coroutines.launch

class CategoriesViewModel(application: Application, private val sharedViewModel: SharedViewModel) :
    BaseViewModel(application) {

    private val _categories = MutableLiveData<List<Category>>()
    val categories: LiveData<List<Category>>
        get() = _categories

    fun getCategories() {
        viewModelScope.launch {
            _categories.value = repository.getCategories()
        }
    }

    private val _toFoodsView = MutableLiveData<Boolean>()
    val toFoodsView: LiveData<Boolean>
        get() = _toFoodsView

    fun toFoods(id: Int) {
        sharedViewModel.currentFoodCategory = id
        _toFoodsView.value = true
    }

    class Factory(
        private val application: Application,
        private val sharedViewModel: SharedViewModel
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CategoriesViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return CategoriesViewModel(application, sharedViewModel) as T
            }
            throw IllegalArgumentException("Unable to construct viewModel")
        }
    }

    fun clear() {
        _toFoodsView.value = false
    }

}