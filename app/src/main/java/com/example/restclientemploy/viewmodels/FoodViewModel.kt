package com.example.restclientemploy.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.example.restclientemploy.domain.Food
import kotlinx.coroutines.launch

class FoodViewModel (application: Application, private val sharedViewModel: SharedViewModel) :
    BaseViewModel(application) {

    private val _foods = MutableLiveData<List<Food>>()
    val foods: LiveData<List<Food>>
        get() = _foods

    fun getFood() {
        viewModelScope.launch {
             _foods.value = repository.getFoodByCategory(sharedViewModel.currentFoodCategory)
        }
    }

    private val _showToast = MutableLiveData<Boolean>()
    val showToast: LiveData<Boolean>
        get() = _showToast

    fun updateShowToast(newValue: Boolean) {
        _showToast.value = newValue
    }

    class Factory(
        private val application: Application,
        private val sharedViewModel: SharedViewModel
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(FoodViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return FoodViewModel(application, sharedViewModel) as T
            }
            throw IllegalArgumentException("Unable to construct viewModel")
        }
    }
}