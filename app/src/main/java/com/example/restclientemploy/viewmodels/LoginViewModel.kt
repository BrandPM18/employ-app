package com.example.restclientemploy.viewmodels

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.launch

class LoginViewModel(application: Application, private val sharedViewModel: SharedViewModel) :
    BaseViewModel(application) {

    var id = 0
    var password = ""

    private val _invalidCredentials = MutableLiveData<Boolean>()
    val invalidCredentials: LiveData<Boolean>
        get() = _invalidCredentials

    fun updateInvalidCredentials(newValue: Boolean) {
        _invalidCredentials.value = newValue
    }

    private val _toSignUp = MutableLiveData<Boolean>()
    val toSignUp: LiveData<Boolean>
        get() = _toSignUp

    private val _successLogin = MutableLiveData<Boolean>()
    val successLogin: LiveData<Boolean>
        get() = _successLogin

    fun login() {
        viewModelScope.launch {
            if (repository.signIn(id, password) != null) {
                _successLogin.value = true
            } else {
                _invalidCredentials.value = true
            }
        }
    }

    class Factory(
        private val application: Application,
        private val sharedViewModel: SharedViewModel
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return LoginViewModel(application, sharedViewModel) as T
            }
            throw IllegalArgumentException("Unable to construct viewModel")
        }
    }

    fun clear() {
        _toSignUp.value = false
        _invalidCredentials.value = false
        _successLogin.value = false
    }

}