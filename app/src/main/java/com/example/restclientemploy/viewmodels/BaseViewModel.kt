package com.example.restclientemploy.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.restclientemploy.repository.EmployRepository

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val repository = EmployRepository()
}